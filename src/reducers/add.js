
export const todoReducer = (state=[], action) => {
	switch(action.type){
		case 'ADD':
			return [...state,{id:action.id,title:action.title,completed:false}]
		
		case 'REMOVE':
			return state.filter(todo => {
				return todo.id !== action.id
			})
		case default:
			return state
	}
}
