
export const add = (title) => {
	return {
		type: 'ADD',
		payload: title,
		id: Math.random()
	}
}

export const remove = (id) => {
	return {
		type: 'REMOVE',
		payload: id
	}
}